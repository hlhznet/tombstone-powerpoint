﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="TombsEntry11.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>P&GD department web master - Kijoo Ahn</h3>
    <address>
        10250 Constellation Blvd. 4th Floor<br />
        Los Angeles, CA 90067<br />
        <abbr title="Phone">P:</abbr>
        818.522.3343
    </address>

    <address>
        <strong>Support eMail:</strong>   <a href="mailto:kahn0821@HL.com">KAhn0821@HL.com</a><br />
    
    </address>
</asp:Content>
