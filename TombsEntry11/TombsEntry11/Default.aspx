﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TombsEntry11._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
       
    <div class="jumbotron">        
        <h1>Presentations & Graphic Design</h1>
        <p><a href="TombControl.aspx" class="btn btn-primary btn-large">Tombstone Control &raquo;</a></p>
    </div>
    <div class="container">
    
        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="5000">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li> 
            <li data-target="#myCarousel" data-slide-to="4"></li>  
            <li data-target="#myCarousel" data-slide-to="5"></li>  
            <li data-target="#myCarousel" data-slide-to="6"></li>  
            <li data-target="#myCarousel" data-slide-to="7"></li>  
            <li data-target="#myCarousel" data-slide-to="8"></li>            
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src=".\Pictures\IMG-4823v2.jpg" alt="Happy Hour" style="width:100%;">
            </div>
            <div class="item">
                <img src=".\Pictures\IMG-4824v2.jpg" alt="Picnic" style="width:100%;">
            </div>
            <div class="item">
                <img src=".\Pictures\IMG-4825v2.jpg" alt="Picnic" style="width:100%;">
            </div>
            <div class="item">
                <img src=".\Pictures\IMG-4826v2.jpg" alt="Picnic" style="width:100%;">
            </div>    
             <div class="item">
                <img src=".\Pictures\IMG-4827v2.jpg" alt="Picnic" style="width:100%;">
            </div>    
             <div class="item">
                <img src=".\Pictures\IMG-4828v2.jpg" alt="Picnic" style="width:100%;">
            </div>    
             <div class="item">
                <img src=".\Pictures\IMG-4829v2.jpg" alt="Picnic" style="width:100%;">
            </div>    
             <div class="item">
                <img src=".\Pictures\IMG-4830v2.jpg" alt="Picnic" style="width:100%;">
            </div>    
             <div class="item">
                <img src=".\Pictures\IMG-4831v2.jpg" alt="Picnic" style="width:100%;">
            </div>        

           
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
        </div>
    </div>
     <div class="header">
	 </div>     
     <td>    </td>
     <a href="Default.aspx">
	 <img src=".\Pictures\HL_Logo.png" width="250" height="20" alt="CSGO Howl" style="float:left";>
	 </a> 	
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-4">
  
        </div>
        <div class="col-md-4">
 
        </div>
    </div>

</asp:Content>
