﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TombControl.aspx.cs" Inherits="TombsEntry11.TombControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Tombstone Metadata Control"></asp:Label>
    <br />
        <asp:ListBox ID="lstFileName" runat="server" Height="151px" OnSelectedIndexChanged="lstFileName_SelectedIndexChanged" Width="597px"></asp:ListBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="BtnNextFile" runat="server" BackColor="#00CC00" Font-Bold="True" OnClick="BtnNextFile_Click" Text="Next File" Width="113px" UseSubmitBehavior="False" />
</p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>File Name</strong> :&nbsp;<asp:TextBox ID="TxtFileName" runat="server" OnTextChanged="TxtFileName_TextChanged" Width="570px" BorderStyle="Solid" Font-Bold="False" ForeColor="#6600FF" Height="22px" MaxLength="1500" Rows="1" CssClass="text-left" Wrap="False"></asp:TextBox>
</p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="font-size: small"><strong>&nbsp;</strong></span><strong>File Type</strong>:&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="TxtFileType" runat="server" OnTextChanged="TxtFileType_TextChanged" style="font-size: small" Width="48px"></asp:TextBox>
    <span style="color: #FF0066"><strong>**</strong></span>&nbsp;&nbsp; <span style="font-size: small"><strong>&nbsp; </strong></span><font size="2"><strong>LOB</strong></font><strong>: </strong>&nbsp;
        <asp:TextBox ID="TxtLOB" runat="server" ReadOnly="True" Width="81px"></asp:TextBox>
&nbsp;&nbsp;
    <asp:DropDownList ID="DdlLOB" runat="server">
        <asp:ListItem Value="  ">    </asp:ListItem>
        <asp:ListItem>CF</asp:ListItem>
        <asp:ListItem>FAS</asp:ListItem>
        <asp:ListItem>FR</asp:ListItem>
        <asp:ListItem>SC</asp:ListItem>
    </asp:DropDownList>
&nbsp;<span style="color: #FF0066"><strong>**&nbsp; </strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Deal Closed Date:
    <asp:TextBox ID="TxtClosedDate" runat="server" style="font-size: small" Width="112px" OnTextChanged="TxtClosedDate_TextChanged"></asp:TextBox>
&nbsp;<span style="color: #FF0066"><strong>**</strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Case No:
    <asp:TextBox ID="TxtCaseNo" runat="server" style="font-size: small" Width="125px"></asp:TextBox>
&nbsp;<span style="color: #FF0066"><strong>**</strong></span></p>


<div class="row">
  <div class="col-xs-6 col-sm-4">MTRA Submitted By:</div>
  <div class="col-xs-6 col-sm-4"><asp:TextBox ID="TextBox3" runat="server" style="font-size: small"></asp:TextBox></div>
  <div class="col-xs-6 col-sm-4"></div>
</div>
<div class="row">
  <div class="col-xs-6 col-sm-4">Deal Type:</div>
  <div class="col-xs-6 col-sm-4"><asp:TextBox ID="TextBox4" runat="server" style="font-size: small"></asp:TextBox>
  <div class="col-xs-6 col-sm-4"></div>
</div>


<%--<table>
    <colgroup>
        <col />
        <col />
    </colgroup>
    <tbody>
        <tr>
            <td>
                MTRA Submitted By:
            </td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" style="font-size: small"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Deal Type:
            </td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" style="font-size: small"></asp:TextBox>
            </td>
        </tr>
    </tbody>
</table>--%>


<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MTRA Submitted By:&nbsp;
    <asp:TextBox ID="TxtSubmitBy" runat="server" style="font-size: small"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Deal Type:<asp:TextBox ID="TxtDealType" runat="server" style="font-size: small"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Role (Tagline):&nbsp;
    <asp:TextBox ID="TxtRole" runat="server" style="font-size: small"></asp:TextBox>
    <span style="color: #FF0066"><strong>**</strong></span></p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MTRA Submit Date :&nbsp;&nbsp;
    <asp:TextBox ID="TxtSubmitDate" runat="server" style="font-size: small"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Legal Client Name:
    <asp:TextBox ID="TxtClientName" runat="server" style="font-size: small" Width="297px"></asp:TextBox>
</p>
<p>
&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Deal Team Approved By:
    <asp:TextBox ID="TxtTeamApprBy" runat="server" style="font-size: small"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Legal Client&#39;s Address:<asp:TextBox ID="TxtClientAddr" runat="server" style="font-size: small" Width="469px"></asp:TextBox>
</p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Deal Team Approval Date:<asp:TextBox ID="TxtTeamApprDate" runat="server" style="font-size: small"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Legal Contra Party:&nbsp;
    <asp:TextBox ID="TxtContraName" runat="server" style="font-size: small" Width="301px"></asp:TextBox>
</p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;Client Approved By:&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="TxtClientApprBy" runat="server" style="font-size: small"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Legal Contra Party Address:
    <asp:TextBox ID="TxtContraAddr" runat="server" style="font-size: small" Width="496px"></asp:TextBox>
</p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Client Approved Date:
    <asp:TextBox ID="TxtClientApprDate" runat="server" style="font-size: small"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Legal Target/Subject Name:
    <asp:TextBox ID="TxtTargetName" runat="server" style="font-size: small" Width="359px"></asp:TextBox>
</p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;PE Approval:&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="TxtPEAppr" runat="server" style="font-size: small"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Legal Target/Subject Address:
    <asp:TextBox ID="TxtTargetAddr" runat="server" style="font-size: small" Width="493px"></asp:TextBox>
</p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Industry Group:
    <asp:TextBox ID="TxtIndGroup" runat="server" style="font-size: small" Width="257px"></asp:TextBox>
    &nbsp;<span style="color: #FF0066"><strong>**</strong></span>&nbsp;&nbsp;&nbsp; Sector:
    <asp:TextBox ID="TxtSector" runat="server" style="font-size: small" Width="415px"></asp:TextBox>
&nbsp;<span style="color: #FF0066; font-size: large"><strong>**</strong></span></p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Deal Amount (in Million USD):
    <asp:TextBox ID="TxtDealAmt" runat="server" style="font-size: small"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Region:
    <asp:TextBox ID="TxtRegion" runat="server" ReadOnly="True"></asp:TextBox>
    <asp:DropDownList ID="DdlRegion" runat="server" Height="16px" style="font-size: small" Width="132px">
        <asp:ListItem Value=" ">  </asp:ListItem>
        <asp:ListItem>Africa</asp:ListItem>
        <asp:ListItem>Asia</asp:ListItem>
        <asp:ListItem>Europe</asp:ListItem>
        <asp:ListItem>North America</asp:ListItem>
        <asp:ListItem>Oceania</asp:ListItem>
        <asp:ListItem>South America</asp:ListItem>
    </asp:DropDownList>
&nbsp;<span style="color: #FF0066"><strong>**</strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Country:
    <asp:TextBox ID="TxtCountry" runat="server" style="font-size: small" Width="165px"></asp:TextBox>
&nbsp;<span style="color: #FF0066"><strong>**</strong></span></p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Comments:&nbsp;
    <asp:TextBox ID="TxtComments" runat="server" style="font-size: small" Width="1073px" Height="22px"></asp:TextBox>
    &nbsp;</p>
<p>
    &nbsp;</p>
<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="BtnSubmit" runat="server" BackColor="#00FFCC" Font-Bold="True" ForeColor="Blue" Text="Submit" Width="137px" OnClick="BtnSubmit_Click" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="BtnExit" runat="server" BackColor="#CC9900" Font-Bold="True" Text="Cancel &amp; Exit" Width="137px" />
</p>
</asp:Content>
