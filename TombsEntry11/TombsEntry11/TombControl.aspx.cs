﻿using System;
using System.Drawing;
using System.ComponentModel;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Web;
using System.Data;
using System.Windows.Controls;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Configuration;
using HL.IntranetLibrary;

namespace TombsEntry11
{
    public partial class TombControl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void TxtFileType_TextChanged(object sender, EventArgs e)
        {
            if(TxtFileType.Text == string.Empty)
            {
                MessageBox.Show ("Please enter a value to TxtFileType field");
                return;
            }
            else
            {
                // program logic
            }
        }

        protected void TxtLOB_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TxtClosedDate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TxtFileName_TextChanged(object sender, EventArgs e)           
        {
            
            string server = ConfigurationManager.AppSettings["ServerName"].ToString();
            string database = ConfigurationManager.AppSettings["Database"].ToString();
            string user = ConfigurationManager.AppSettings["UserName"].ToString();

            string sConnString = SupportFunctions.GetPowerpointTombstoneConnection("", server, database, user);
            string sCString = "Data Source=" 
            + server
            + ";Initial Catalog="
            + database
            + ";User ID="
            + user 
            + ";Password=";

            sCString = sCString + sConnString.Substring(sConnString.IndexOf(";Password=") + 10, (sConnString.Length - sConnString.IndexOf(";Password=") - 11));
            string sqlStatement = "Select * from TombstoneMaster "
            + "Where FileName LIKE '"
            + TxtFileName.Text.Replace("'","''")
            + "%' ORDER by FileName";

            using (SqlConnection conn = new SqlConnection(sCString))
            using (SqlCommand cmd = new SqlCommand(sqlStatement, conn))
            {
                lstFileName.Items.Clear();
                conn.Open();                             
                using (SqlDataReader rd = cmd.ExecuteReader())
                {
                    while (rd.Read())
                    {
                        lstFileName.Items.Add(new ListItem((string)rd ["FileName"]));
                    }                    
                }
            }
            
        }

        protected void BtnNextFile_Click(object sender, EventArgs e)
        {
            string server = ConfigurationManager.AppSettings["ServerName"].ToString();
            string database = ConfigurationManager.AppSettings["Database"].ToString();
            string user = ConfigurationManager.AppSettings["UserName"].ToString();

            string sConnString = SupportFunctions.GetPowerpointTombstoneConnection("", server, database, user);
            string sCString = "Data Source=" 
            + server
            + ";Initial Catalog="
            + database
            + ";User ID="
            + user 
            + ";Password=";

            sCString = sCString + sConnString.Substring(sConnString.IndexOf(";Password=") + 10, (sConnString.Length - sConnString.IndexOf(";Password=") - 11));
            string tombstoneName = TxtFileName.Text;
            string sqlStatement = "Select TOP 1 * from TombstoneMaster "
            + "Where FileName > '"
            + tombstoneName.Replace("'","''")
            + "' Order by FileName ";
            TxtSubmitDate.Text = "               ";
            TxtTeamApprDate.Text = "               ";
            TxtClientApprDate.Text = "               ";
            TxtClosedDate.Text = "               ";
            TxtDealAmt.Text = "               ";

            lstFileName.SelectedIndex = lstFileName.SelectedIndex + 1;

            using (SqlConnection conn = new SqlConnection(sCString))
            using (SqlCommand cmd = new SqlCommand(sqlStatement, conn))
            {
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                rdr.Read();

                TxtFileName.Text = rdr.GetString(2);
                TxtFileType.Text = rdr.GetString(3);
                TxtLOB.Text = rdr.GetString(11);
                if (!rdr.IsDBNull(5))
                    TxtSubmitDate.Text = rdr.GetDateTime(5).ToString("yyyy-MM-dd");
                if (!rdr.IsDBNull(7))
                    TxtTeamApprDate.Text = rdr.GetDateTime(7).ToString("yyyy-MM-dd");
                if (!rdr.IsDBNull(9))
                    TxtClientApprDate.Text = rdr.GetDateTime(9).ToString("yyyy-MM-dd");
                if (!rdr.IsDBNull(13))
                    TxtClosedDate.Text = rdr.GetDateTime(13).ToString("yyyy-MM-dd");
                if (!rdr.IsDBNull(27))
                    //if (!string.IsNullOrEmpty(rdr.GetValue(27).ToString())) 
                    TxtDealAmt.Text = rdr.GetValue(27).ToString();
                TxtCaseNo.Text = rdr.GetString(12);
                TxtSubmitBy.Text = rdr.GetString(4);
                TxtTeamApprBy.Text = rdr.GetString(6);
                TxtClientApprBy.Text = rdr.GetString(8);
                TxtPEAppr.Text = rdr.GetString(10);
                TxtDealType.Text = rdr.GetString(14);
                TxtClientName.Text = rdr.GetString(15);
                TxtClientAddr.Text = rdr.GetString(16);
                TxtContraName.Text = rdr.GetString(17);
                TxtContraAddr.Text = rdr.GetString(18);
                TxtTargetName.Text = rdr.GetString(19);
                TxtTargetAddr.Text = rdr.GetString(20);
                TxtRole.Text = rdr.GetString(21);
                TxtIndGroup.Text = rdr.GetString(22);
                TxtSector.Text = rdr.GetString(23);
                TxtRegion.Text = rdr.GetString(25);
                TxtCountry.Text = rdr.GetString(26);
                TxtComments.Text = rdr.GetString(28);
                return;
            }            
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {

        }

        protected void lstFileName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sqlconn = "Server=slv-haldevdb;Database=Tombstone;User Id=tombdba;Password=SForcedev1";
            string tombstoneName = lstFileName.SelectedItem.ToString();
            string sqlStatement = "Select * from TombstoneMaster "
            + "Where FileName = '"
            + tombstoneName
            + "' ";
            
            using (SqlConnection conn = new SqlConnection(sqlconn))
            {
                SqlCommand newCmd = new SqlCommand(sqlStatement, conn);

                conn.Open();
                SqlDataReader rdr = newCmd.ExecuteReader();
                rdr.Read();

                TxtFileName.Text = rdr.GetString(2); 
                TxtFileType.Text = rdr.GetString(3);
                TxtLOB.Text = rdr.GetString(11);
                if (!rdr.IsDBNull(5))
                   TxtSubmitDate.Text = rdr.GetDateTime(5).ToString("yyyy-MM-dd");
                if (!rdr.IsDBNull(7))
                   TxtTeamApprDate.Text = rdr.GetDateTime(7).ToString("yyyy-MM-dd");
                if (!rdr.IsDBNull(9))
                   TxtClientApprDate.Text = rdr.GetDateTime(9).ToString("yyyy-MM-dd");
                if (!rdr.IsDBNull(13))
                   TxtClosedDate.Text = rdr.GetDateTime(13).ToString("yyyy-MM-dd");
                if (!rdr.IsDBNull(27))
                //if (!string.IsNullOrEmpty(rdr.GetValue(27).ToString())) 
                   TxtDealAmt.Text = rdr.GetValue(27).ToString();
                TxtCaseNo.Text = rdr.GetString(12);
                TxtSubmitBy.Text = rdr.GetString(4);
                TxtTeamApprBy.Text = rdr.GetString(6);
                TxtClientApprBy.Text = rdr.GetString(8);
                TxtPEAppr.Text = rdr.GetString(10);
                TxtDealType.Text = rdr.GetString(14);
                TxtClientName.Text = rdr.GetString(15);
                TxtClientAddr.Text = rdr.GetString(16);
                TxtContraName.Text = rdr.GetString(17);
                TxtContraAddr.Text = rdr.GetString(18);
                TxtTargetName.Text = rdr.GetString(19);
                TxtTargetAddr.Text = rdr.GetString(20);
                TxtRole.Text = rdr.GetString(21);
                TxtIndGroup.Text = rdr.GetString(22);
                TxtSector.Text = rdr.GetString(23);
                TxtRegion.Text = rdr.GetString(25);
                TxtCountry.Text = rdr.GetString(26);
                TxtComments.Text = rdr.GetString(28);
                return;
            }            
        }
               
    }
}