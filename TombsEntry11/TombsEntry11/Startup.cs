﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TombsEntry11.Startup))]
namespace TombsEntry11
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
